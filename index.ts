/// <reference types="parse" />
import { LINQ } from "berish-linq";

export default async function execute<T extends Parse.Object>(query: Parse.Query<T>, queryOptions?: ParseQueryOptions) {
    let options = getOptions(queryOptions);
    let skip = 0;
    const limit = 1000;
    let objects: Parse.Object[] = [];
    let count = await query.count(options.count);
    let done = false;
    while (!done) {
        let temp = await query.ascending('createdAt').limit(limit).skip(skip).find(options.find);
        objects = objects.concat(temp);
        skip += limit;
        if (skip > count)
            done = true;
        if (skip > 10000) {
            skip = 0;
            let createdAt = temp[temp.length - 1].createdAt;
            query = query.greaterThanOrEqualTo('createdAt', createdAt);
            if (objects.length >= count)
                done = true;
        }
    }
    return LINQ.fromArray(objects as T[]);
}

function getOptions(options?: ParseQueryOptions) {
    let opt: ParseQueryAllOptions = {};
    if (options == null) {
        opt = {
            count: undefined,
            each: undefined,
            find: undefined,
            first: undefined,
            get: undefined
        };
    } else {
        let countOptions: Parse.Query.CountOptions = {
            useMasterKey: options.useMasterKey,
            error: options.error,
            sessionToken: options.sessionToken,
            success: options.success
        };
        let eachOptions: Parse.Query.EachOptions = {
            useMasterKey: options.useMasterKey,
            error: options.error,
            sessionToken: options.sessionToken,
            success: options.success
        };
        let findOptions: Parse.Query.FindOptions = {
            useMasterKey: options.useMasterKey,
            error: options.error,
            sessionToken: options.sessionToken,
            success: options.success
        };
        let firstOptions: Parse.Query.FirstOptions = {
            useMasterKey: options.useMasterKey,
            error: options.error,
            sessionToken: options.sessionToken,
            success: options.success
        };
        let getOptions: Parse.Query.GetOptions = {
            useMasterKey: options.useMasterKey,
            error: options.error,
            sessionToken: options.sessionToken,
            success: options.success
        };
        opt = {
            count: countOptions,
            each: eachOptions,
            find: findOptions,
            first: firstOptions,
            get: getOptions
        };
    }
    return opt;
}

export interface ParseQueryOptions {
    error?: Function;
    success?: Function;
    useMasterKey?: boolean;
    sessionToken?: string;
}

interface ParseQueryAllOptions {
    count?: Parse.Query.CountOptions;
    each?: Parse.Query.EachOptions;
    find?: Parse.Query.FindOptions;
    first?: Parse.Query.FirstOptions;
    get?: Parse.Query.GetOptions;
}